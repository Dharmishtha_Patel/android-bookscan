package com.example.jaypatel.bookscan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Jay patel on 3/27/2018.
 */

public class ListAdapter extends ArrayAdapter<book_list> {

    public ArrayList<book_list> MainList;

    public ArrayList<book_list> BookListTemp;

   // public ListAdapter.SubjectDataFilter BookDataFilter ;

    public ListAdapter(Context context, int id, ArrayList<book_list> subjectArrayList) {

        super(context, id, subjectArrayList);

        this.BookListTemp = new ArrayList<book_list>();

        this.BookListTemp.addAll(subjectArrayList);

        this.MainList = new ArrayList<book_list>();

        this.MainList.addAll(subjectArrayList);
    }

  /*  @Override
    public Filter getFilter() {

        if (BookDataFilter == null){

            BookDataFilter  = new ListAdapter.SubjectDataFilter();
        }
        return BookDataFilter;
    }*/


    public class ViewHolder {

        TextView patronName;
        TextView bookName;
        TextView Return_status;
        TextView Author_name;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListAdapter.ViewHolder holder = null;

        if (convertView == null) {

            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = vi.inflate(R.layout.list_item, null);

            holder = new ListAdapter.ViewHolder();

            holder.patronName = (TextView) convertView.findViewById(R.id.patron1);

            holder.bookName = (TextView) convertView.findViewById(R.id.book_name);
            holder.Return_status = (TextView) convertView.findViewById(R.id.returnstatus);
            holder.Author_name=(TextView)convertView.findViewById(R.id.author);
            convertView.setTag(holder);

        } else {
            holder = (ListAdapter.ViewHolder) convertView.getTag();
        }

        book_list subject = BookListTemp.get(position);

        holder.patronName.setText(subject.getBook_name());

        holder.bookName.setText(subject.getUser_name());
        holder.Return_status.setText(subject.getReturn_status());
        holder.Author_name.setText(subject.getAuthor_name());

        return convertView;

    }

   /* private class SubjectDataFilter extends Filter
    {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {

            charSequence = charSequence.toString().toLowerCase();

            FilterResults filterResults = new FilterResults();

            if(charSequence != null && charSequence.toString().length() > 0)
            {
                ArrayList<book_list> arrayList1 = new ArrayList<book_list>();

                for(int i = 0, l = MainList.size(); i < l; i++)
                {
                    book_list bookList = MainList.get(i);

                    if(bookList.toString().toLowerCase().contains(charSequence))

                        arrayList1.add(bookList);
                }
                filterResults.count = arrayList1.size();

                filterResults.values = arrayList1;
            }
            else
            {
                synchronized(this)
                {
                    filterResults.values = MainList;

                    filterResults.count = MainList.size();
                }
            }
            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

            BookListTemp = (ArrayList<book_list>)filterResults.values;

            notifyDataSetChanged();

            clear();

            for(int i = 0, l = BookListTemp.size(); i < l; i++)
                add(BookListTemp.get(i));

            notifyDataSetInvalidated();
        }
    }*/

}
