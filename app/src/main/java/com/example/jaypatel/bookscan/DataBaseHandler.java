package com.example.jaypatel.bookscan;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 07-03-2018.
 */
public class DataBaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = " barcode_db";

    // Contacts table name
    private static final String TABLE_CONTACTS = " barcode_table_data";

    // Contacts Table Columns names
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "barcode_id";
    public static final String KEY_DATE = "return_date";
    public static final String KEY_barcode = "key_barcode";



    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT" + "," + KEY_DATE + " TEXT" +"," + KEY_barcode + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }
    void addContact(NoteModel contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.barcode_id);
        values.put(KEY_DATE,contact.Return);
        values.put(KEY_barcode,contact.book_id);

        // Contact Phone

        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        db.close(); // Closing database connection
    }


    public List<NoteModel> getAllContacts() {
        List<NoteModel> contactList = new ArrayList<NoteModel>();
        // Select All Query
        String selectQuery =  "SELECT * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                NoteModel contact = new NoteModel();
                contact.set_id(Integer.valueOf(cursor.getString(0)));
                contact.setBarcode_id(cursor.getString(1));
                contact.setReturn(cursor.getString(2));
                contact.setBook_id(cursor.getString(3));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        // close inserting data from database
        db.close();
        // return contact list
//        Log.d("image valueee", String.valueOf(cursor.getBlob(4)));
        Log.d("value", String.valueOf(contactList));
        return contactList;
    }

    public void deleteContact() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS,null,null);
        db.close();
    }

    public void deleteCustomer(String book_name){
       // String.valueOf(this.getWritableDatabase().delete(TABLE_CONTACTS,KEY_NAME + "=?" , new String[]{book_name+""}));
        Log.d("bookname123",book_name);
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(TABLE_CONTACTS,KEY_NAME+"="+ book_name,null);
        db.execSQL("DELETE FROM"+TABLE_CONTACTS+ " WHERE "+KEY_barcode+"='"+book_name+"'");
        db.close();
    }

}
