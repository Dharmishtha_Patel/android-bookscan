package com.example.jaypatel.bookscan;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jay patel on 3/27/2018.
 */

public class book_list {


    @SerializedName("book_name")
    String book_name;

    @SerializedName("user_name")
    String user_name;

    @SerializedName("Return_status")
    String Return_status;
    @SerializedName("author_name")
    String author_name;
    @SerializedName("userdata")
    private List<book_list> user_data;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public book_list (String b_name, String patron_name){
        super();
        this.book_name = b_name;

    }
    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getReturn_status() {
        return Return_status;
    }

    public void setReturn_status(String return_status) {
        Return_status = return_status;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }



    public List<book_list> getUser_data() {
        return user_data;
    }

    public void setUser_data(List<book_list> user_data) {
        this.user_data = user_data;
    }



}
