package com.example.jaypatel.bookscan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.HashMap;

public class Login extends AppCompatActivity {

    EditText User_name, Password;
    Button LogIn;
    Button button_camera;
    String PasswordHolder, EmailHolder;
    String message;
    String barcode;
    String BarcodeHolder;
    String full_name;
    String fname,lname,name,email;
    String finalResult ;
    //String HttpURL = "http://192.168.2.215/Barcode/login.php";
    String HttpURL =  "http://beacontree.net/Library_barcode/login.php";
    Boolean CheckEditText;
    ProgressDialog progressDialog;
    HashMap<String,String> hashMap = new HashMap<>();
    public static final String UserEmail = "";
    public static final String Username = "";
    public static final String mypreference = "mypref";
    public static final String Name = "nameKey";
    public static final String Email = "emailKey";
    SharedPreferences sharedpreferences;
    DataBaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        User_name = (EditText)findViewById(R.id.email);
        button_camera = (Button)findViewById(R.id.button_camera);
        // Password = (EditText)findViewById(R.id.password);
      //  LogIn = (Button)findViewById(R.id.Login);


        button_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this,User_scanner.class);
                startActivity(intent);
            }
        });


        Intent intent = getIntent();
        barcode = intent.getStringExtra("result");

        User_name.setText(barcode);

        /*LogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CheckEditTextIsEmptyOrNot();

                if(CheckEditText){
                    //getSqlDetails();
                    UserLoginFunction(BarcodeHolder);

                }
                else {

                    Toast.makeText(Login.this, "Please fill all form fields.", Toast.LENGTH_LONG).show();
                }
            }
        });*/
    }

    public void CheckEditTextIsEmptyOrNot(){

        BarcodeHolder = User_name.getText().toString();
        // PasswordHolder = Password.getText().toString();

        if(TextUtils.isEmpty(BarcodeHolder))
        {
            CheckEditText = false;
        }
        else {
            CheckEditText = true ;
        }
    }
   /* private void getSqlDetails() {

        String url= "http://192.168.2.215/Barcode/login_data.php?user_name="+EmailHolder;
        //String url = "http://beacontree.net/Library_barcode/login_data.php?user_name="+EmailHolder;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //  pd.hide();
                        try {
                            JSONArray jsonarray = new JSONArray(response);

                            for(int i=0; i < jsonarray.length(); i++) {

                                JSONObject jsonobject = jsonarray.getJSONObject(i);

                                fname = jsonobject.getString("first_name").trim();
                                lname = jsonobject.getString("last_name").trim();
                                email = jsonobject.getString("user_email").trim();

                                name = fname +" "+lname;
                                System.out.println(name);


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error != null){

                            Toast.makeText(getApplicationContext(), "/Something went wrong.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void SavePreferences(String key, String value){
        SharedPreferences sharedPreferences = getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void LoadPreferences(){
        SharedPreferences sharedPreferences = getSharedPreferences("MY_SHARED_PREF", MODE_PRIVATE);
        String strSavedMem1 = sharedPreferences.getString("MEM1", "");
        String strSavedMem2 = sharedPreferences.getString("MEM2", "");
        //textSavedMem1.setText(strSavedMem1);
        //textSavedMem2.setText(strSavedMem2);
    }
*/
//   z

   /* public void OnReturn(View view)
    {
        startActivity(new Intent(Login.this,MainActivityclass));
    }
    }*/

}
