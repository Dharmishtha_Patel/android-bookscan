package com.example.jaypatel.bookscan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jay patel on 3/6/2018.
 */

public class SettingAccount extends Fragment  {
//implements ExampleDialog.ExampleDialogListener

    TextView bookname,returnstatus,author;
    Bundle args;
    ListView listView;
    SearchView editText;
    List<book_list> BookList ;
    ListAdapter listAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.setting, container, false);


        listView = (ListView) rootView.findViewById(R.id.listview1);

        editText = (SearchView) rootView.findViewById(R.id.edit);

        editText.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                listAdapter.getFilter().filter(newText.toString());
                return false;
            }
        });

     /*   FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(SettingAccount.this).attach(SettingAccount.this).commit();*/
        BookList = new ArrayList<>();

     //  listView.setTextFilterEnabled(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            bookname = (TextView)view.findViewById(R.id.patron1);
                returnstatus=(TextView)view.findViewById(R.id.returnstatus);
                author=(TextView)view.findViewById(R.id.author);
                String book_name,return_status,authorname;
                book_name= bookname.getText().toString();
                return_status=returnstatus.getText().toString();
                Log.d("hhh",book_name+return_status);
               if(return_status.equals("1")) {


                 Intent a = new Intent(getActivity(),UserBook.class);
                   a.putExtra("bookname", book_name);
                   a.putExtra("return_status", return_status);

                   startActivity(a);


                }
                else if(return_status.equals("0")){
                   Intent a = new Intent(getActivity(),AvailableBook.class);
                   a.putExtra("bookname", book_name);
                   a.putExtra("return_status", return_status);
                   a.putExtra("author",author.getText().toString());
                   startActivity(a);
               }

            }
        });



        ApiService service = api.getClient().create(ApiService.class);
        Call<book_list> call = service.getbookDetails();
        call.enqueue(new Callback<book_list>() {
            @Override
            public void onResponse(Call<book_list> call, Response<book_list> response) {
                if(response.isSuccessful()) {
                    Log.d("dattatat", response.body().getUser_data().get(0).getBook_name().toString());
                    BookList = response.body().getUser_data();
                    listAdapter = new ListAdapter(getActivity(), R.layout.list_item, (ArrayList<book_list>) BookList);
                    listView.setAdapter(listAdapter);
                }
                else {
                    Log.d("errord",response.message());
                }
            }

            @Override
            public void onFailure(Call<book_list> call, Throwable t) {
            Log.d("error",t.getMessage());
            }
        });

       /* editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                listAdapter.getFilter().filter(cs.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                listAdapter.getFilter().filter(arg0.toString());
            }
        });*/
     //   new ParseJSonDataClass(getActivity()).execute();

        return rootView;
    }


}

