package com.example.jaypatel.bookscan;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.jaypatel.bookscan.data.CheckoutList;
import com.google.zxing.Result;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;

@RequiresApi(api = Build.VERSION_CODES.N)
public class scanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;
    private static int camId = Camera.CameraInfo.CAMERA_FACING_BACK;
    String name, email;
    public static final String Name = "";
    public static final String Email = "";
    String barcode_id,user_email;
    SQLiteDatabase sqLiteDatabase;
    DataBaseHandler db;
private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd ");
    String HttpJSonURL;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
//        int currentApiVersion = Build.VERSION.SDK_INT;
//        if (currentApiVersion >= Build.VERSION_CODES.M) {
//            if (checkPermission()) {
//                Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();
//            } else {
//                requestPermission();
//            }
//        }
/*

        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("UserName");
        email = bundle.getString("Useremail");
*/


    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        scannerView.setResultHandler(this);
        scannerView.startCamera();
        /*int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        *//*scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        scannerView.setResultHandler(this);
        scannerView.startCamera();*//*
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (scannerView == null) {

                }

            } else {
                requestPermission();
            }
        }*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(scanner.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void handleResult(final Result result) {
       /* final String myResult = result.getText();
        Log.d("QRCodeScanner", result.getText());
        Log.d("QRCodeScanner", result.getBarcodeFormat().toString());

        HttpJSonURL = "http://192.168.2.215/Barcode/borrow.php?barcode_id=`" + myResult;
        final String myresult1 = result.getText();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm Scan");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Book_detail fragment = new Book_detail();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.commit();
                //scannerView.resumeCameraPreview(MainActivity.this);
                Intent intent = new Intent(scanner.this, MainActivity.class);
                intent.putExtra("result", myresult1);
                *//*intent.putExtra("Name", name);
                startActivity(intent);
                intent.putExtra("Email", email);*//*
                startActivity(intent);

            }
        });
    }*/
       /* barcode_id = result.getText();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm Scan");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //    db= new DataBaseHandler(BarcodeActivity.this);
                //  db.addContact(new NoteModel(barcode_id ));
                ApiService service = api.getClient().create(ApiService.class);
                Call<DataModel> call = service.getAllTicketDetails();
                call.enqueue(new Callback<DataModel>() {
                    @Override
                    public void onResponse(Call<DataModel> call, final Response<DataModel> response) {

                        Log.d("response",response.body().getData().toString());
                        Log.d("valuebook",response.body().toString());
                        Log.d("ressss",response.body().getData().get(0).getBarcode_id());
                        for(int i=0; i<response.body().getData().size();i++ ) {

                            if (response.body().getData().get(i).getBarcode_id().equals(barcode_id)) {
                                Calendar c = Calendar.getInstance();
                                c.add(Calendar.DATE,15);
                                Date currentDatePlusOne = c.getTime();

                                String newDate = dateFormat.format(currentDatePlusOne);

                                Log.d("fff",response.body().getData().get(i).getBook_name());
                                db= new DataBaseHandler(scanner.this);
                                db.addContact(new NoteModel(response.body().getData().get(i).getBook_name() ,newDate));
                                Log.d("success","success");

                               Intent i1 = new Intent(scanner.this, MainActivity.class);
                                startActivity(i1);
                        }
                            *//*else {
                                //Toast.makeText(getApplicationContext(),"No avilable in Database",Toast.LENGTH_LONG).show();
                                Intent i1 = new Intent(scanner.this, MainActivity.class);
                                startActivity(i1);
                            }*//*
                        }

                    }

                    @Override
                    public void onFailure(Call<DataModel> call, Throwable t) {
                        // Log error here since request failed
                        Log.e("error", t.toString());
                        Toast.makeText(getApplicationContext(),"No Data Found",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
        AlertDialog alert1 = builder.create();
        alert1.show();
        scannerView.stopCamera();*/

        barcode_id = result.getText();
        ApiService service = api.getClient().create(ApiService.class);
        Call<DataModel> call = service.getAllTicketDetails();
        call.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, final Response<DataModel> response) {

                Log.d("response",response.body().getData().toString());
                Log.d("valuebook",response.body().toString());
                Log.d("ressss",response.body().getData().get(0).getBarcode_id());
                for(int i=0; i<response.body().getData().size();i++ ) {

                    if (response.body().getData().get(i).getBarcode_id().equals(barcode_id)) {
                        Calendar c = Calendar.getInstance();
                        c.add(Calendar.DATE,15);
                        Date currentDatePlusOne = c.getTime();

                        String newDate = dateFormat.format(currentDatePlusOne);

                        Log.d("fff",response.body().getData().get(i).getBook_name());

//                        Book_detail.setNewBarCodeData(response.body().getData().get(i).getBook_name() ,
//                                newDate,barcode_id);
                        CheckoutList.setData(response.body().getData().get(i).getBook_name() ,
                                newDate,barcode_id);
//                        db= new DataBaseHandler(scanner.this);
//                        db.addContact(new NoteModel(response.body().getData().get(i).getBook_name() ,newDate,barcode_id));
                        Log.d("success","success");
                        AlertDialog.Builder builder = new AlertDialog.Builder(scanner.this);
                        builder.setTitle(response.body().getData().get(i).getBook_name());
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //    db= new DataBaseHandler(BarcodeActivity.this);
                                //  db.addContact(new NoteModel(barcode_id ));
                                Intent i1 = new Intent(scanner.this, MainActivity.class);
                                startActivity(i1);
                            }
                        });
                        AlertDialog alert1 = builder.create();
                        alert1.show();

                    }
                            /*else {
                                //Toast.makeText(getApplicationContext(),"No avilable in Database",Toast.LENGTH_LONG).show();
                                Intent i1 = new Intent(scanner.this, MainActivity.class);
                                startActivity(i1);
                            }*/
                }

            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                // Log error here since request failed
                Log.e("error", t.toString());
                Toast.makeText(getApplicationContext(),"No Data Found",Toast.LENGTH_LONG).show();
            }
        });


        scannerView.stopCamera();

    }

    }

