package com.example.jaypatel.bookscan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Admin on 07-03-2018.
 */
public class ListDisplayAdapter extends ArrayAdapter<NoteModel> {

    Context context;
    public    byte[] outImage;
    public Bitmap theImage2;
    int layoutResourceId;
    DataBaseHandler dataBaseHandler;
    String name;
    ArrayList<NoteModel> data=new ArrayList<NoteModel>();
    public ListDisplayAdapter(Context context, int layoutResourceId, ArrayList<NoteModel> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ImageHolder holder = null;
            dataBaseHandler =  new DataBaseHandler(context);
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ImageHolder();
            holder.txtTitle = (TextView)row.findViewById(R.id.text1);
            holder.date1 = (TextView)row.findViewById(R.id.date);
            holder.add = (TextView)row.findViewById(R.id.add);
            //holder.barcode_id =(TextView)row.findViewById(R.id.barcode_id);
            holder.btn_delete = (Button) row.findViewById(R.id.btn_delete);

            row.setTag(holder);
        }
        else
        {
            holder = (ImageHolder)row.getTag();
        }

        final NoteModel picture = data.get(position);
        holder.txtTitle.setText(picture.barcode_id);
//        holder.barcode_id.setText(picture.book_id);
        holder.date1.setText(picture.Return);

Log.d("notedata",picture.barcode_id+picture.Return+picture.book_id);

        final ImageHolder finalHolder = holder;
        notifyDataSetChanged();
        holder.btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = finalHolder.txtTitle.getText().toString();
                Log.d("nameofbook",name);
                remove(picture);
                Log.d("all", String.valueOf(dataBaseHandler.getAllContacts()));
                    dataBaseHandler.deleteCustomer(picture.book_id);
                Log.d("delete", String.valueOf(dataBaseHandler.getAllContacts()));
                //clear();
notifyDataSetChanged();

              //  Intent refresh = new Intent(context,MainActivity.class);
            }
        });
        return row;
    }

    static class ImageHolder
    {
        TextView date1;
        TextView txtTitle;
        ImageView i1;
        TextView barcode_id;
        TextView lat,lon,add;
        Button btn_delete;
    }
}

