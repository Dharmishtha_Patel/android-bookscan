package com.example.jaypatel.bookscan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;

public class User_scanner extends AppCompatActivity implements ZXingScannerView.ResultHandler{

        private static final int REQUEST_CAMERA = 1;
        private ZXingScannerView scannerView;
        private static int camId = Camera.CameraInfo.CAMERA_FACING_BACK;
    ApiService service;
    Call<DataModel> call,call1;
    AlertDialog.Builder builder;
    DataBaseHandler db;
    public static final String mypreference = "mypref";
    SharedPreferences sharedpreferences;
    public static final String Barcode = "barcode";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_scanner);

        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        if (!checkPermission()) {
            requestPermission();
        }
        int currentApiVersion = Build.VERSION.SDK_INT;
//        if (currentApiVersion >= Build.VERSION_CODES.M) {
//            if (checkPermission()) {
//                Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();
//            } else {
//                requestPermission();
//            }
//        }
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    private void initiateScanner() {
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }
    @Override
    public void onResume() {
        super.onResume();
        initiateScanner();
        /*int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        *//*scannerView = new ZXingScannerView(this);
        setContentView(scannerView);
        scannerView.setResultHandler(this);
        scannerView.startCamera();*//*
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (scannerView == null) {
            } else {
                requestPermission();
            }
        }*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot " +
                                "access camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(User_scanner.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void handleResult(final Result result) {

        final String myResult = result.getText();
        Log.d("QRCodeScanner", result.getText());
        Log.d("QRCodeScanner", result.getBarcodeFormat().toString());


        final String myresult1 = result.getText();
        service = api.getClient().create(ApiService.class);
        call1 = service.getUser(myresult1);
        call1.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {


                builder = new AlertDialog.Builder(User_scanner.this);
                try {
                    builder.setTitle("Welcome  "+response.body().getUser_data().get(0)
                                .getFirst_name());
                    sharedpreferences = getSharedPreferences(mypreference,
                            Context.MODE_PRIVATE);
                    if (sharedpreferences.contains(Barcode)) {
                        sharedpreferences.getString(Barcode, "");
                    }
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(Barcode, myresult1);
                    editor.commit();
                    builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            Intent intent = new Intent(User_scanner.this, MainActivity.class);
                            // intent.putExtra("result", myresult1);
                            startActivity(intent);

                        }
                    });
                    builder.setMessage(result.getText());
                    AlertDialog alert1 = builder.create();
                    alert1.show();
                    scannerView.stopCamera();

                } catch (IndexOutOfBoundsException e) {
                    builder.setTitle("Error");
                    builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            initiateScanner();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            User_scanner.this.finish();
                        }
                    });
                    builder.setMessage("Invalid barcode");
                    AlertDialog alert1 = builder.create();
                    alert1.show();
                }
                
                
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {

            }
        });


    }
    }

