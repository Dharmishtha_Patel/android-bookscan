package com.example.jaypatel.bookscan;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Admin on 12-03-2018.
 */

public interface ApiService {


    @POST("borrow.php")
    Call<DataModel> getAllTicketDetails();

    @POST("book_detail.php")
    Call<book_list> getbookDetails();

    @FormUrlEncoded
    @POST("login_data.php")
    Call<DataModel> getUser(@Field("user_barcode_id") String user_id);

    @FormUrlEncoded
    @POST("User_borrow.php")
    Call<DataModel> getbook_detail(@Field("book_name") String book_name,@Field("Return_status") String Return_status);

    @FormUrlEncoded
    @POST("login.php")
    Call<DataModel> UserAuthenticate(@Field("user_barcode_id") String user_id);

    @FormUrlEncoded
    @POST("borrow_detail.php")
    Call<DataModel> insertdata(@Field("user_name") String user_name,@Field("book_barcode_id") String book_barcode_id,@Field("book_name") String book_name,@Field("return_date") String return_date);

}
