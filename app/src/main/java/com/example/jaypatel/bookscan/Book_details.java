package com.example.jaypatel.bookscan;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 12-03-2018.
 */

public class Book_details {
    @SerializedName("book_name")
    String book_name;
    @SerializedName("barcode_id")
    String barcode_id;
    @SerializedName("user_barcode_id")
    String user_barcode_id;

    @SerializedName("first_name")
    String first_name;
    @SerializedName("user_name")
    String user_name;

    @SerializedName("return_date")
    String return_date;



    @SerializedName("user_email")
    String user_email;


    public String getUser_name() {
        return user_name;
    }

    public String getReturn_date() {
        return return_date;
    }

    public void setReturn_date(String return_date) {
        this.return_date = return_date;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }


    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getBarcode_id() {
        return barcode_id;
    }

    public void setBarcode_id(String barcode_id) {
        this.barcode_id = barcode_id;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }
    public String getUser_barcode_id() {
        return user_barcode_id;
    }

    public void setUser_barcode_id(String user_barcode_id) {
        this.user_barcode_id = user_barcode_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }
}
