package com.example.jaypatel.bookscan;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 24-04-2018.
 */
public class UserBook extends Activity {
Bundle args;
    String bookname,return_status;
    ApiService service;
    Call<DataModel> call,call1;
    TextView user,returndate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userbook);


        bookname = getIntent().getStringExtra("bookname").toString();
        return_status = getIntent().getStringExtra("return_status").toString();
 user= (TextView)findViewById(R.id.user);
        returndate= (TextView)findViewById(R.id.returndate);

        Toast.makeText(getBaseContext(),bookname+return_status,Toast.LENGTH_LONG).show();
        service = api.getClient().create(ApiService.class);
        call = service.getbook_detail(bookname,return_status);
        call.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {

               String user_name = response.body().getUser_data().get(0).getUser_name();
                    user.setText(bookname+"is borrowed by "+user_name);
                  returndate.setText("Return date:"+response.body().getUser_data().get(0).getReturn_date());
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {

            }
        });

    }

}
