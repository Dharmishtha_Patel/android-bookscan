package com.example.jaypatel.bookscan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jaypatel.bookscan.data.CheckoutList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jay patel on 3/6/2018.
 */

public class Book_detail extends Fragment {

    ListView dataList;
    private static NoteModel newBarCodeData;
    private static boolean isNewDataReceived = false;
    ArrayList<NoteModel> imageArry;
    ListDisplayAdapter imageAdapter;
    public DataBaseHandler db;
    ArrayList<DataModel> imageArry1 = new ArrayList<DataModel>();
    List<Book_details> booklist;
    String user_name,book_id,return_date,bbok_name,book_name,email,user_email,msg;
    TextView barcode;
    private RelativeLayout relativeLayout;
    DataModel daaa;
    ListView listview;
    Button btn_delete;
    String aaa;
    FrameLayout coordinatorLayout;
    FloatingActionButton fab;
    ApiService service;
    Call<DataModel> call,call1;
    String name;
    ImageView stnimage;
    List<String> list = new ArrayList<String>();
    public static final String mypreference = "mypref";
    SharedPreferences sharedpreferences;
    public static final String Barcode = "barcode";
    String barcodenumber;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.book_detail, container, false);
//        relativeLayout =(RelativeLayout)rootView.findViewById(R.id.relativelayout);

        coordinatorLayout = (FrameLayout)rootView.findViewById(R.id.relativelayout);
        fab = rootView.findViewById(R.id.fab);
        stnimage = rootView.findViewById(R.id.strimg);
        Log.d("NewData", "flag initialized");
        //barcode = (TextView)rootView.findViewById(R.id.barcode);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/

                Intent intent = new Intent(getActivity(), scanner.class);
                startActivity(intent);
            }
        });

        dataList = (ListView) rootView.findViewById(R.id.listview);
        db = new DataBaseHandler(getActivity());
        booklist = new ArrayList<>();
        final List<NoteModel> contacts = db.getAllContacts();
//        for(int i =0 ; i<contacts.size();i++){
//            book_id= contacts.get(i).getBook_id();
//            Log.d("bbbbbbbbbbbbbbbbbbbb",book_id);
//        }


        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Barcode)) {
            barcodenumber=sharedpreferences.getString(Barcode, "");
            Log.d("barcodenumber",barcodenumber);
        }




        service = api.getClient().create(ApiService.class);
        call = service.getUser(barcodenumber);
        //call = service.getUser(email);
        call.enqueue(new Callback<DataModel>() {
            @Override
            public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                user_name = response.body().getUser_data().get(0).getFirst_name();
                user_email = response.body().getUser_data().get(0).getUser_email();
                //Log.d("daaa",response.body().getUser_data().get(0).getUser_email());
            }

            @Override
            public void onFailure(Call<DataModel> call, Throwable t) {
                Log.d("error",t.getMessage());
            }
        });
        Log.d("hi", "hiii");
        daaa = new DataModel();
//        imageArry.clear();
//        imageArry.addAll(contacts);

        imageAdapter = new ListDisplayAdapter(getActivity(), R.layout.screen_display,
                CheckoutList.getData());
        dataList.setAdapter(imageAdapter);
        dataList.invalidate();
        imageAdapter.notifyDataSetChanged();
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, "Click here to send data", Snackbar.LENGTH_INDEFINITE)
                .setAction("SEND", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       // sendMail();
                        StringBuilder productNamesBuilder = new StringBuilder();
                        for(int i = 0; i < contacts.size(); i++){
                            bbok_name= contacts.get(i).getBarcode_id();
                            book_id = contacts.get(i).getBook_id();
                            return_date= contacts.get(i).getReturn();
                            service = api.getClient().create(ApiService.class);
                            productNamesBuilder.append("Book Name:"+bbok_name ).append("\n");
                            productNamesBuilder.append(" "+book_id).append("\ngyvhgvbnv bnvbv");
                            call1 = service.insertdata(user_name,book_id,bbok_name,return_date);
                            call1.enqueue(new Callback<DataModel>() {
                                @Override
                                public void onResponse(Call<DataModel> call, Response<DataModel> response) {
                                    Log.d("response",response.body().toString());
                                    Log.d("success","success");
                                }
                                @Override
                                public void onFailure(Call<DataModel> call, Throwable t) {
                                }
                            });

                        }
                        Toast.makeText(getContext(),productNamesBuilder,Toast.LENGTH_LONG).show();
                        Log.d("vvvv", String.valueOf(productNamesBuilder));
                        String[] strings = productNamesBuilder.toString().split("  ");
                        msg= productNamesBuilder.toString()+"\ngvhjhbjb";
                        sendMail(msg);

                        Log.d("data",name+book_id+return_date);
                        //  Snackbar snackbar1 = Snackbar.make(coordinatorLayout, "Data send", Snackbar.LENGTH_SHORT);
                        db.deleteContact();
                        imageAdapter.clear();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Thank you for Borrowing Books");
                        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent a = new Intent(getActivity(),Login.class);
                                startActivity(a);
                            }
                        });
                        builder.setNegativeButton("Continue",new DialogInterface.OnClickListener(){

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent a = new Intent(getActivity(),MainActivity.class);

                                startActivity(a);
                            }
                        });
                        AlertDialog alert1 = builder.create();
                        alert1.show();
                        //Toast.makeText(getActivity(),"Data send",Toast.LENGTH_LONG).show();

                    }
                });
        View view = snackbar.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);
        snackbar.getView().setBackgroundColor(getActivity().getResources().getColor(R.color.scaner_btn));
        snackbar.setActionTextColor(Color.WHITE);
        snackbar.show();



        return rootView;
    }

//    public static void setNewBarCodeData(String barcode_id, String aReturn, String book_id) {
//        Log.d("NewData", "set successful!");
//        newBarCodeData = new NoteModel(barcode_id, aReturn, book_id);
//        isNewDataReceived = true;
//        Log.d("NewDataFlag", String.valueOf(isNewDataReceived));
//    }

//    public static void updateImageArryAndNotify(String barcode_id, String aReturn, String book_id) {
//        newBarCodeData = new NoteModel(barcode_id, aReturn, book_id);
//
//    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d("NewData", "Array size: " + MainActivity.getData().size());
    }

    private void sendMail(String msge) {

        String subject = "Regarding of borrowing a book from library";
        //String message1 = "You borrowed "+bookname+",written by "+authorname+".";
        String message2 =  "<html>" +
                "<center><hr> Welcome "+user_name+"</hr></center></html>";

        SendMail sm = new SendMail(getContext(), user_email, subject, msg);

        sm.execute();
    }


}
